;;; ce-better-defaults.el --- init function for emacs           -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Christopher Eames

;; Author: Christopher Eames <chream>


;;;###autoload
(defun ce-init-startup ()
  ;; Turn off mouse interface early in startup to avoid momentary display
  (setq user-full-name "Christopher Eames")
  (if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
  (if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
  (if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
  (setq custom-file (expand-file-name "custom.el" user-emacs-directory))
  (when (file-exists-p custom-file)
    (load custom-file)))




(provide 'ce-better-defaults)


;; (defvar ce-cache-directory (expand-file-name "~/.cache/emacs/"))
;; (unless (file-exists-p ce-cache-directory)
;;   (make-directory ce-cache-directory))

;; No splash screen please ... jeez
;; (setq inhibit-startup-message t)

;; ;; store all backup and autosave files in the tmp dir
;; (setq backup-directory-alist
;;       `((".*" . ,ce-cache-directory)))
;; (setq auto-save-file-name-transforms
;;        `((".*" ,ce-cache-directory t)))


;; (defvar pre-init-fns (list #'ce-init-startup))
;; (defvar post-init-fns (list #'ce-init-osx-env
;;                             #'ce-init-killring
;;                             #'ce-init-visual
;;                             #'ce-init-misc
;;                             #'ce-init-keybindings))



;; ;;;###autoload
;; (defun ce-init-visual ()
;;   (setq-default truncate-lines t)
;;   (cond ((eq system-type 'gnu/linux)
;;          (set-frame-font "DejaVu Sans Mono"))
;;         ((eq system-type 'darwin)
;;          (set-frame-font "Source Code Pro"))
;;         ((eq system-type 'windows-nt)
;;          (set-frame-font "Lucida Sans Typewriter"))))

;; ;;;###autoload
;; (defun ce-init-keybindings ()
;;   ;; set leader key
;;   (setq ce-main-leader "M-m")
;;   ;; unset needed key
;;   (global-unset-key (kbd "M-m"))
;;   ;; Annoing Keybindings.
;;   (global-unset-key (kbd "C-<right>"))
;;   (global-unset-key (kbd "C-<left>"))
;;   (global-unset-key (kbd "C-z"))

;;   (keyboard-translate ?\- ?\@)
;;   (keyboard-translate ?\@ ?\-)
;;   (keyboard-translate ?\C-@ ?\/))



;; ;;;###autoload
;; (defun ce-init-all ()
;;   (cl-dolist (init-fn pre-init-fns)
;;     (funcall init-fn))
;;   (cl-dolist (init-fn post-init-fns)
;;     (add-hook 'after-init-hook init-fn)))

;; ;;;###autoload
;; (defun ce-init-osx-env ()
;;   ;; ;; https://www.emacswiki.org/emacs/EmacsForMacOS
;;   ;; (set-terminal-coding-system 'utf-8)
;;   ;; (set-keyboard-coding-system 'utf-8)
;;   ;;  (prefer-coding-system 'utf-8)



;;   ;; ;; http://stuff-things.net/2015/10/05/emacs-visible-bell-work-around-on-os-x-el-capitan/
;;   ;; ;; some osx bug
;;   ;; (setq visible-bell nil)
;;   ;; (setq ring-bell-function 'ignore)

;;   ;; ;; low level debugger.
;;   ;; (setq gdb-many-windows t
;;   ;;       gdb-show-main t)
;;)
