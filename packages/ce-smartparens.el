;;; ce-smartparens.el --- a simple package                     -*- lexical-binding: t; -*-

;;  Copyright (C) 2017  Christopher Eames

;; Author: Christopher Eames <chream>
;; Keywords: lisp, extensions
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; Commentary:
;; Put a description of the package here
;; Code:


;;;###autoload
(defmacro def-pairs (pairs)
  "https://ebzzry.github.io/emacs-pairs.html#manipulation"
  `(progn
     ,@(cl-loop for (key . val) in pairs
             collect
             `(defun ,(read (concat
                             "ce-wrap-with-"
                             (prin1-to-string key)
                             "s"))
                  (&optional arg)
                (interactive "p")
                (sp-wrap-with-pair ,val)))))

###autoload
(defun ce-init-smartparens ()
  "."
  (with-eval-after-load 'smartparens
    (def-pairs ((paren        . "(")
                (sbracket      . "[")
                (brace        . "{")
                (single-quote . "'")
                (double-quote . "\"")
                (back-quote   . "`")))))


(provide 'ce-smartparens)

;;; ce-smartparens.el ends here
