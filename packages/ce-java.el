;;; ce-java.el --- a simple package                     -*- lexical-binding: t; -*-

;;  Copyright (C) 2017  Christopher Eames

;; Author: Christopher Eames <chream>
;; Keywords: lisp, extensions
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Put a description of the package here

;;; Code:

;;;###autoload
(defun eclim-run-test ()
  (interactive)
  (if (not (string= major-mode "java-mode"))
      (message "Sorry cannot run current buffer."))
  (compile (concat eclim-executable
                   " -command java_junit -p "
                   eclim--project-name " -t "
                   (eclim-package-and-class))))

(provide 'ce-java)
