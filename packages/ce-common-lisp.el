;;; ce-common-lisp.el --- a simple package                     -*- lexical-binding: t; -*-

;;  Copyright (C) 2017  Christopher Eames

;; Author: Christopher Eames <chream>
;; Keywords: lisp, extensions
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Put a description of the package here

;;; Code:

;;;###autoload
(defun ce-dir-list-packages ()
  (remove-if (lambda (file)
               (or (string-match-p "#" file)
                   (string-match-p "all$$" file)))
             (mapcar #'create-package-name
                     (ce-dir-list-files ".lisp"))))

;;;###autoload
(defun ce-int-list-insert (list)
  (interactive)
  (cl-loop for package in list
           for count = 1 then (incf count)
           do (progn
    (insert package)
                (if (= count (length list))
                    nil
                  (newline-and-indent)))))

;;;###autoload
(defun slime-description-fontify ()
  "Fontify sections of SLIME Description."
  (with-current-buffer "*SLIME Description*"
    (highlight-regexp
     (concat "^Function:\\|"
             "^Macro-function:\\|"
             "^Its associated name.+?) is\\|"
             "^The .+'s arguments are:\\|"
             "^Function documentation:$\\|"
             "^Its.+\\(is\\|are\\):\\|"
             "^On.+it was compiled from:$")
     'hi-green-b)))

;;;###autoload
(defadvice slime-show-description (after slime-description-fontify activate)
  "Fontify sections of SLIME Description."
  (slime-description-fontify))

;; (setq sp-autoskip-closing-pair 'always-end)

(provide 'ce-common-lisp)
