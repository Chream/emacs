;;; ce-buffers.el --- Tools relating to buffers      -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Christopher Eames

;; Author: Christopher Eames <izzy@macc.local>
;; Keywords: lisp
;; Commentary:
;; Code:


;;;###autoload
(defun split-windows-terminal ()
  "Split into 3 evenly."
  (interactive)
  (save-excursion
    (let ((ps (window-height)))
      (split-window-below (- ps (/ ps 5))))))

;;;###autoload
(defun split-vertically-75 ()
  "Split buffer to 75 width."
  (interactive)
  (save-excursion
    (split-window-right 75)))

;;;###autoload
(defun revert-buffer-no-confirm ()
  "Revert buffer without confirmation."
  (interactive) (revert-buffer t t))

;; http://nullprogram.com/blog/2010/10/06/
;;;###autoload
(defun set-buffer-width (n)
  "Set the selected window's width."
  (adjust-window-trailing-edge (selected-window) (- n (window-width)) t))

;;;###autoload
(defun set-75-columns ()
  "Set the selected window to 75 columns."
  (interactive)
  (set-buffer-width 75))

;;;###autoload
(defun set-81-columns ()
  "Set the selected window to 81 columns."
  (interactive)
  (set-buffer-width 81))

;;;###autoload
(defun set-101-columns ()
  "Set the selected window to 101 columns."
  (interactive)
  (set-buffer-width 101))

;;;###autoload
(defun toggle-selective-display (column)
  (interactive "P")
  (set-selective-display
   (or column
       (unless selective-display
         (1+ (current-column))))))

;;;###autoload
(defun toggle-hiding (column)
  (interactive "P")
  (if hs-minor-mode
      (if (condition-case nil
              (hs-toggle-hiding)
            (error t))
          (hs-show-all))
    (toggle-selective-display column)))

;;;###autoload
(defun ce-cleanup-buffer-safe ()
  "Perform a bunch of safe operations on the whitespace content of a buffer.
Does not indent buffer, because it is used for a before-save-hook, and that
might be bad."
  (interactive)
  (untabify (point-min) (point-max))
  (delete-trailing-whitespace)
  (set-buffer-file-coding-system 'utf-8))

;;;###autoload
(defun ce-cleanup-buffer ()
  "Perform a bunch of operations on the whitespace content of a buffer.
Including indent-buffer, which should not be called automatically on save."
  (interactive)
  (ce-cleanup-buffer-safe)
  (indent-region (point-min) (point-max)))

;;;###autoload
(defun ce-frame-fullscreen ()
  (interactive)
  (let ((fullscreen '(frame-parameter nil 'fullscreen)))
    (modify-frame-parameters
     nil '((fullscreen . fullboth) (fullscreen-restore . ,fullscreen)))))


;; https://github.com/ninrod/dotfiles/blob/master/emacs/boot.org#ninrodorigami-toggle-node

;;;###autoload
(defun ninrod/origami-toggle-node ()
  (interactive)
  (save-excursion ;; leave point where it is
    (goto-char (point-at-eol))             ;; then go to the end of line
    (origami-toggle-node (current-buffer) (point))))


;; ;; fix tag jump
;; ;; Implementing my own copy of this function since it is required by
;; ;; semantic-ia-fast-jump but this function is not defined in etags.el
;; ;; of GNU emacs
;; (require 'etags)
;; (unless (fboundp 'push-tag-mark)
;;   (defun push-tag-mark ()
;;     "Push the current position to the ring of markers so that
;;     \\[pop-tag-mark] can be used to come back to current position."
;;     (interactive)
;;     (ring-insert find-tag-marker-ring (point-marker))
;;     )
;;   )

(provide 'ce-buffers)
