;;; ce-key-bindings.el --- a simple package         -*- lexical-binding: t; -*-

;;  Copyright (C) 2017  Christopher Eames

;; Author: Christopher Eames <chream>
;; Keywords: lisp, extensions
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
                                        ;

;;; Commentary:

;; Put a description of the package here

;;; Code:

;; Global keymap.

(defun ergoemacs-select-text-in-quote ()
  "Select text between the nearest left and right delimiters.
Delimiters are paired characters:
 () [] {} «» ‹› “” 〖〗 【】 「」 『』 （） 〈〉 《》 〔〕 ⦗⦘ 〘〙 ⦅⦆ 〚〛 ⦃⦄ ⟨⟩
 For practical purposes, also: \"\", but not single quotes."
  (interactive)
  (let (p1)
    (skip-chars-backward "^<>([{“「『‹«（〈《〔【〖⦗〘⦅〚⦃⟨\"'")
    (setq p1 (point))
    (skip-chars-forward "^<>)]}”」』›»）〉》〕】〗⦘〙⦆〛⦄⟩\"'")
    (set-mark p1)))

;;;###autoload
(defvar ergoemacs-lvl0-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "M-i") 'previous-line)
    (define-key map (kbd "M-k") 'next-line)
    (define-key map (kbd "M-j") 'backward-word)
    (define-key map (kbd "M-l") 'forward-word)

    (define-key map (kbd "C-M-i") 'backward-paragraph)
    (define-key map (kbd "C-M-k") 'forward-paragraph)
    (define-key map (kbd "C-M-j") 'backward-char)
    (define-key map (kbd "C-M-l") 'forward-char)

    (define-key map (kbd "C-M-u") 'backward-sexp)
    (define-key map (kbd "C-M-o") 'forward-sexp)
    map))

(define-minor-mode ergoemacs-lvl0-mode
  "A minor mode for lvl1 ergoemacs key bindings."
  :init-value t
  :lighter lvl0
  :keymap ergoemacs-lvl0-mode-map)

(defvar ergoemacs-lvl1-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "M-r") 'sp-delete-word)
    (define-key map (kbd "M-e") 'sp-backward-delete-word)
    (define-key map (kbd "M-f") 'sp-delete-char)
    (define-key map (kbd "M-d") 'sp-backward-delete-char)
    (define-key map (kbd "M-<backspace>") 'sp-backward-delete-char)
    (define-key map (kbd "<backspace>") 'ce-backward-delete-char-or-word)
    map))

(define-minor-mode ergoemacs-lvl1-mode
  "A minor mode for lvl1 ergoemacs key bindings."
  :init-value t
  :lighter lvl1
  :keymap ergoemacs-lvl1-mode-map)

(defvar ergoemacs-lvl2-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "M-c") 'whole-line-or-region-kill-ring-save)
    (define-key map (kbd "M-x") 'whole-line-or-region-kill-region)
    (define-key map (kbd "M-v") 'whole-line-or-region-yank)
    (define-key map (kbd "C-M-v") 'counsel-yank-pop)
    (define-key map (kbd "M-z") 'undo-tree-undo)
    (define-key map (kbd "M-b") 'undo-tree-redo)
    (define-key map (kbd "M-u") 'undo-tree-visualize)
    (define-key map (kbd "M-n") 'beginning-of-buffer)
    (define-key map (kbd "C-M-n") 'end-of-buffer)
    map))

(define-minor-mode ergoemacs-lvl2-mode
  "A minor mode for lvl1 ergoemacs key bindings."
  :init-value t
  :lighter lvl2
  :keymap ergoemacs-lvl2-mode-map)

;;;###autoload
(defvar mgkb-minor-mode-map
  (let ((map (make-sparse-keymap)))

    ;; (define-key map (kbd "C-c k") 'duplicate-line-or-region)
    ;; (define-key map (kbd "M-y") 'counsel-yank-pop)

    ;; Snippetsk
    (define-key map (kbd "M-ø") 'hippie-expand)
    (define-key map (kbd "C-t") 'name-of-the-file-interactive)
    ;; (define-key map (kbd "M-y") 'helm-show-kill-ring)
    ;; Buffers.
    (define-key map (kbd "<f5>") 'revert-buffer-no-confirm)
    (define-key map (kbd "C-c 3") 'split-vertically-75)
    ;; http://nullprogram.com/blog/2010/10/06/
    (define-key map (kbd "M-m b c")     'set-81-columns)
    (define-key map (kbd "M-m b d")     'set-101-columns)
    (define-key map (kbd "<backtab>")   'fold-dwim-toggle)

    (define-key map (kbd "C-ø") (lambda () (interactive)
                                  (whack-whitespace t)))

    (define-key map (kbd "C-M-@") (kbd "\\"))
    ;; Highlighting
    (define-key map (kbd "M-m t 8") 'highlight-long-lines-80)
    (define-key map (kbd "M-m t 9") 'highlight-long-lines-100)

    ;; parens
    (define-key map (kbd "C-æ") 'sp-forward-slurp-sexp)
    (define-key map (kbd "M-æ") 'sp-forward-barf-sexp)
    (define-key map (kbd "C-å") 'sp-backward-slurp-sexp)
    (define-key map (kbd "M-å") 'sp-backward-barf-sexp)

    (define-key map (kbd "H-å") (kbd "{"))
    (define-key map (kbd "H-æ") (kbd "}"))
    (define-key map (kbd "H-p") (kbd "["))
    (define-key map (kbd "H-ø") (kbd "]"))
    (define-key map (kbd "å") (kbd "("))
    (define-key map (kbd "æ") (kbd ")"))

    ;; jumping
    (define-key map (kbd "M-.") 'spacemacs/jump-to-definition)
    (define-key map (kbd "M-,") 'evil-jump-backward)
    (define-key map (kbd "C-M-,") 'evil-jump-forward)

    (define-key map (kbd  "C-c <SPC>") 'avy-goto-word-1)
    (define-key map (kbd  "C-x <SPC>") 'avy-goto-char-2)

    (define-key map (kbd "C-M-w") 'sp-unwrap-sexp)
    (define-key map (kbd "C-d") 'keyboard-quit)
    (define-key map (kbd "C-M-H") 'ergoemacs-select-text-in-quote)
    ;; (define-key map (kbd "C-c å")       'ce-wrap-with-parens)
    ;; (define-key map (kbd "C-c M-å")     'ce-wrap-with-brackets)
    ;; (define-key map (kbd "C-c C-M-å")   'ce-wrap-with-braces)
    ;; (define-key map (kbd "C-c '")       'ce-wrap-with-single-quotes)
    ;; (define-key map (kbd "C-c \"")      'ce-wrap-with-double-quotes)
    map)
  "my-keys-minor-mode keymap.")


;;;###autoload
(define-minor-mode mgkb-minor-mode
  "A minor mode so that my key settings override annoying major modes."
  :init-value t
  :lighter "mgk"
  :keymap mgkb-minor-mode-map)


;; Lisp keymap.
;;;###autoload
(defvar lisp-keys-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "M-<f9>") 'macrostep-expand)
    (define-key map (kbd "C-<f9>") 'macrostep-collapse)
    (define-key map (kbd "C-c M-r") 'slime-restart-inferior-lisp)
    (define-key map (kbd "C-c C-ø") 'slime-connect)
    map))

;;;###autoload
(define-minor-mode lisp-keys-mode
  "A minor mode so that my key settings override annoying major modes."
  :init-value nil
  :lighter "mgk"
  :keymap lisp-keys-mode-map)


;;;###autoload
(defun ce-latex-bindings ()
  (local-set-key (kbd "{")
                 (lambda ()
                   (interactive)
                   (insert (kbd "\\")
                           (kbd "{")
                           (kbd "\\")
                           (kbd "}"))
                   (backward-char 2))))


;; Utils.
;;;###autoload
(defun mgkb-have-priority (_file)
  "Try to ensure that my keybindings retain priority over other minor modes.
   Called via the `after-load-functions' special hook."
  (unless (eq (caar minor-mode-map-alist) 'mgkb-minor-mode)
    (let ((mykeys (assq 'mgkb-minor-mode minor-mode-map-alist)))
      (assq-delete-all 'mgkb-minor-mode minor-mode-map-alist)
      (add-to-list 'minor-mode-map-alist mykeys))))

;; (mgkb-have-priority)

(provide 'ce-key-bindings)
