;;; ce-rcirc.el --- init function for emacs           -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Christopher Eames

;; Author: Christopher Eames <chream>

(eval-after-load 'rcirc
  (defun-rcirc-command reconnect (arg)
    "Reconnect the server process."
    (interactive "i")
    (if (buffer-live-p rcirc-server-buffer)
        (with-current-buffer rcirc-server-buffer
          (let ((reconnect-buffer (current-buffer))
                (server (or rcirc-server rcirc-default-server))
                (port (if (boundp 'rcirc-port) rcirc-port rcirc-default-port))
                (nick (or rcirc-nick rcirc-default-nick))
                channels)
            (dolist (buf (buffer-list))
              (with-current-buffer buf
                (when (equal reconnect-buffer rcirc-server-buffer)
                  (remove-hook 'change-major-mode-hook
                               'rcirc-change-major-mode-hook)
                  (let ((server-plist (cdr (assoc-string server rcirc-server-alist))))
                    (when server-plist
                      (setq channels (plist-get server-plist :channels))))
                  )))
            (if process (delete-process process))
            (rcirc-connect server port nick
                           "Chream"
                           nil
                           channels "Monsterp1kk"))))))

;;; Attempt reconnection at increasing intervals when a connection is
;;; lost.

(defvar rcirc-reconnect-attempts 0)

;;;###autoload
(define-minor-mode rcirc-reconnect-mode
  :init-value nil
  :lighter " Auto-Reconnect"
  :after-hook
  (if rcirc-reconnect-mode
      (progn
        (make-local-variable 'rcirc-reconnect-attempts)
        (add-hook 'rcirc-sentinel-hooks
                  'rcirc-reconnect-schedule nil t))
    (remove-hook 'rcirc-sentinel-hooks
                 'rcirc-reconnect-schedule t)))

(defun rcirc-reconnect-schedule (process &optional sentinel seconds)
  (condition-case err
      (when (and (eq 'closed (process-status process))
                 (buffer-live-p (process-buffer process)))
        (with-rcirc-process-buffer process
          (unless seconds
            (setq seconds (exp (1+ rcirc-reconnect-attempts))))
          (rcirc-print
           process "my-rcirc.el" "ERROR" rcirc-target
           (format "scheduling reconnection attempt in %s second(s)." seconds) t)
          (run-with-timer
           seconds
           nil
           'rcirc-reconnect-perform-reconnect
           process)))
    (error
     (rcirc-print process "RCIRC" "ERROR" nil
                  (format "%S" err) t)))
  )

(defun rcirc-reconnect-perform-reconnect (process)
  (when (and (eq 'closed (process-status process))
             (buffer-live-p (process-buffer process))
             )
    (with-rcirc-process-buffer process
      (when rcirc-reconnect-mode
        (if (get-buffer-process (process-buffer process))
            ;; user reconnected manually
            (setq rcirc-reconnect-attempts 0)
          (let ((msg (format "attempting reconnect to %s..."
                             (process-name process)
                             )))
            (rcirc-print process "my-rcirc.el" "ERROR" rcirc-target
                         msg t))
          ;; remove the prompt from buffers
          (condition-case err
              (progn
                (save-window-excursion
                  (save-excursion
                    (rcirc-cmd-reconnect nil)))
                (setq rcirc-reconnect-attempts 0))
            ((quit error)
             (incf rcirc-reconnect-attempts)
             (rcirc-print process "my-rcirc.el" "ERROR" rcirc-target
                          (format "reconnection attempt failed: %s" err)  t)
             (rcirc-reconnect-schedule process))))))))


(provide 'ce-rcirc)

;;; ce-rcirc.el ends here
