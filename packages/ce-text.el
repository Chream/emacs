;;; ce-text.el --- Tools for editing text            -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Christopher Eames

;; Author: Christopher Eames <chream>
;; Keywords: lisp, tools, extensions
;; Commentary:
;; Code:

;;;###autoload
(defun copy-line (arg)
  "Copy `ARG' lines (as many as prefix argument) in the kill ring.
Ease of use features:
   - Move to start of next line.
   - Appends the copy on sequential calls.
   - Use newline as last char even on the last line of the buffer.
   - If region is active, copy its lines."
  (interactive "p")
  (let ((beg (line-beginning-position))
        (end (line-end-position arg)))
    (when mark-active
      (if (> (point) (mark))
          (setq beg (save-excursion (goto-char (mark)) (line-beginning-position)))
        (setq end (save-excursion (goto-char (mark)) (line-end-position)))))
    (if (eq last-command 'copy-line)
        (kill-append (buffer-substring beg end) (< end beg))
      (kill-ring-save beg end)))
  (kill-append "\n" nil))

;;;###autoload
(defun duplicate-line-or-region (&optional n)
  "Duplicate current line, or region if active.
    With argument N, make N copies.
    With negative N, comment out original line and use the absolute value."
  (interactive "*p")
  (let ((use-region (use-region-p)))
    (save-excursion
      (let ((text (if use-region        ;Get region if active, otherwise line
                      (buffer-substring (region-beginning) (region-end))
                    (prog1 (thing-at-point 'line)
                      (end-of-line)
                      (if (< 0 (forward-line 1)) ;Go to beginning of next line, or make a new one
                          (newline))))))
        (dotimes (i (abs (or n 1)))     ;Insert N times, or once if not specified
          (insert text))))
    (if use-region nil                  ;Only if we're working with a line (not a region)
      (let ((pos (- (point) (line-beginning-position)))) ;Save column
        (if (> 0 n)                             ;Comment out original with negative arg
            (comment-region (line-beginning-position) (line-end-position)))
        (forward-line 1)
        (forward-char pos)))))

;; https://github.com/Fuco1/.emacs.d/blob/af82072196564fa57726bdbabf97f1d35c43b7f7/site-lisp/redef.el#L20-L94

;;;###autoload
(defun Fuco1/lisp-indent-function (indent-point state)
  "This function is the normal value of the variable `lisp-indent-function'.
The function `calculate-lisp-indent' calls this to determine
if the arguments of a Lisp function call should be indented specially.

INDENT-POINT is the position at which the line being indented begins.
Point is located at the point to indent under (for default indentation);
STATE is the `parse-partial-sexp' state for that position.

If the current line is in a call to a Lisp function that has a non-nil
property `lisp-indent-function' (or the deprecated `lisp-indent-hook'),
it specifies how to indent.  The property value can be:

* `defun', meaning indent `defun'-style
  \(this is also the case if there is no property and the function
  has a name that begins with \"def\", and three or more arguments);

* an integer N, meaning indent the first N arguments specially
  (like ordinary function arguments), and then indent any further
  arguments like a body;

* a function to call that returns the indentation (or nil).
  `lisp-indent-function' calls this function with the same two arguments
  that it itself received.

This function returns either the indentation to use, or nil if the
Lisp function does not specify a special indentation."
  (let ((normal-indent (current-column))
        (orig-point (point)))
    (goto-char (1+ (elt state 1)))
    (parse-partial-sexp (point) calculate-lisp-indent-last-sexp 0 t)
    (cond
     ;; car of form doesn't seem to be a symbol, or is a keyword
     ((and (elt state 2)
           (or (not (looking-at "\\sw\\|\\s_"))
               (looking-at ":")))
      (if (not (> (save-excursion (forward-line 1) (point))
                  calculate-lisp-indent-last-sexp))
          (progn (goto-char calculate-lisp-indent-last-sexp)
                 (beginning-of-line)
                 (parse-partial-sexp (point)
                                     calculate-lisp-indent-last-sexp 0 t)))
      ;; Indent under the list or under the first sexp on the same
      ;; line as calculate-lisp-indent-last-sexp.  Note that first
      ;; thing on that line has to be complete sexp since we are
      ;; inside the innermost containing sexp.
      (backward-prefix-chars)
      (current-column))
     ((and (save-excursion
             (goto-char indent-point)
             (skip-syntax-forward " ")
             (not (looking-at ":")))
           (save-excursion
             (goto-char orig-point)
             (looking-at ":")))
      (save-excursion
        (goto-char (+ 2 (elt state 1)))
        (current-column)))
     (t
      (let ((function (buffer-substring (point)
                                        (progn (forward-sexp 1) (point))))
            method)
        (setq method (or (function-get (intern-soft function)
                                       'lisp-indent-function)
                         (get (intern-soft function) 'lisp-indent-hook)))
        (cond ((or (eq method 'defun)
                   (and (null method)
                        (> (length function) 3)
                        (string-match "\\`def" function)))
               (lisp-indent-defform state indent-point))
              ((integerp method)
               (lisp-indent-specform method state
                                     indent-point normal-indent))
              (method
               (funcall method indent-point state))))))))

;; https://emacs.stackexchange.com/questions/30401/backward-kill-word-kills-too-much-how-to-make-it-more-intelligent

;;;###autoload
(defun ce-backward-kill-char-or-word ()
  (interactive)
  (with-eval-after-load 'smartparens
    (cond
     ((looking-back (rx (char word)) 1)
      (sp-backward-kill-word 1))
     ((looking-back (rx (char blank)) 1)
      (delete-horizontal-space t))
     (t
      (sp-backward-delete-char 1)))))

;;;###autoload
(defun ce-backward-delete-char-or-word ()
  (interactive)
  (with-eval-after-load 'smartparens
    (cond
     ((looking-back (rx (char word)) 1)
      (sp-backward-delete-word 1))
     ((looking-back (rx (char blank)) 1)
      (delete-horizontal-space t))
     (t
      (sp-backward-delete-char 1)))))

;;;###autoload
(defun name-of-the-file-interactive ()
  "Gets the name of the file the current buffer is based on."
  (interactive)
  (insert (replace-regexp-in-string "/Users/izzy/repos/" "" (buffer-file-name
                                                             (window-buffer
                                                              (minibuffer-selected-window))))))

;;;###autoload
(defun name-of-the-file ()
  "Gets the name of the file the current buffer is based on."
  (replace-regexp-in-string "/Users/izzy/repos/" "" (buffer-file-name
                                                     (window-buffer
                                                      (minibuffer-selected-window)))))

;;;###autoload
(defun create-package-name (&optional file)
  "Gets the package name from the file."
  (concat ":" (string-remove-suffix ".lisp"
               (string-remove-prefix "/Users/izzy/repos/" (if file
                                                              file
                                                            (name-of-the-file))))))

;;;###autoload
(defun ce-current-directory ()
  "Gets the current directory."
  (string-remove-prefix "Directory " (pwd)))

(defadvice kill-line (after kill-line-cleanup-whitespace activate compile)
  "cleanup whitespace on kill-line"
  (if (not (bolp))
      (delete-region (point) (progn (skip-chars-forward " \t") (point)))))

;;;###autoload
(defun whack-whitespace (arg)
  "Delete all white space from point to the next word.  With prefix ARG
    delete across newlines as well.  The only danger in this is that you
    don't have to actually be at the end of a word to make it work.  It
    skips over to the next whitespace and then whacks it all to the next
    word."
  (interactive "P")
  (let ((regexp (if arg "[ \t\n]+" "[ \t]+")))
    (re-search-forward regexp nil t)
    (replace-match "" nil nil)))

;;;###autoload
(defun highlight-long-lines-100 ()
  (interactive)
  (spacemacs/toggle-highlight-long-lines 100))

;;;###autoload
(defun highlight-long-lines-80 ()
  (interactive)
  (spacemacs/toggle-highlight-long-lines 80))

(provide 'ce-text)
