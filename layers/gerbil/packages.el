;;; packages.el --- gerbil layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2018 Sylvain Benner & Contributors
;;
;; Author: Christopher Eames <izzy@macc.local>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;;; Commentary:

;; See the Spacemacs documentation and FAQs for instructions on how to implement
;; a new layer:
;;
;;   SPC h SPC layers RET
;;
;;
;; Briefly, each package to be installed or configured by this layer should be
;; added to `gerbil-packages'. Then, for each package PACKAGE:
;;
;; - If PACKAGE is not referenced by any other Spacemacs layer, define a
;;   function `gerbil/init-PACKAGE' to load and initialize the package.

;; - Otherwise, PACKAGE is already referenced by another Spacemacs layer, so
;;   define the functions `gerbil/pre-init-PACKAGE' and/or
;;   `gerbil/post-init-PACKAGE' to customize the package as it is loaded.

;;; Code:

(defconst gerbil-packages
  '((gambit :location (recipe :url "https://github.com/gambit/"
                              :getcher git
                              :files ("misc/gambit.el")))
    (gerbil :location (recipe :url "https://github.com/vyzo/gerbil.git"
                              :fetcher git
                              :files ("src/etc/gerbil.el")) )
    company-etags)
  "The list of Lisp packages required by the gerbil layer.")


(defun gerbil/post-init-company-etags ())

(defun gerbil/init-gambit ()
  (use-package gambit
    :defer t))

(defun gerbil/init-gerbil ()
  (use-package gerbil
    :defer t))


;;; packages.el ends here
