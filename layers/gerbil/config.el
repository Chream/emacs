;;; config.el --- Gerbil Layer configuration File for Spacemacs
;;
;; Copyright (c) 2018 Christopher Eames
;;
;; Author: Christopher Eames <chream@gmx.com>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;; Variables

(spacemacs|define-jump-handlers gerbil-mode gerbil-edit-definition)
