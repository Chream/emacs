;;; packages.el --- ce layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2017 Sylvain Benner & Contributors
;;
;; Author: Christopher Eames <izzy@1x-193-157-177-246.uio.no>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;;; Commentary:

;; See the Spacemacs documentation and FAQs for instructions on how to implement
;; a new layer:
;;
;;   SPC h SPC layers RET
;;
;;
;; Briefly, each package to be installed or configured by this layer should be
;; added to `ce-packages'. Then, for each package PACKAGE:
;;
;; - If PACKAGE is not referenced by any other Spacemacs layer, define a
;;   function `ce/init-PACKAGE' to load and initialize the package.

;; - Otherwise, PACKAGE is already referenced by another Spacemacs layer, so
;;   define the functions `ce/pre-init-PACKAGE' and/or
;;   `ce-better-defualts/post-init-PACKAGE' to customize the package as it is loaded.

;;; Code:
(defconst ce-packages
  '((ce-better-defaults :location (recipe :url "https://Chream@bitbucket.org/Chream/emacs.git"
                                          :fetcher git
                                          :files ("packages/ce-better-defaults.el")))
    (ce-files :location (recipe :url "https://Chream@bitbucket.org/Chream/emacs.git"
                                :fetcher git
                                :files ("packages/ce-files.el")))
    (ce-common-lisp :location (recipe :url "https://Chream@bitbucket.org/Chream/emacs.git"
                                      :fetcher git
                                      :files ("packages/ce-common-lisp.el")))
    (ce-java :location (recipe :url "https://Chream@bitbucket.org/Chream/emacs.git"
                               :fetcher git
                               :files ("packages/ce-java.el")))
    (ce-buffers :location (recipe :url "https://Chream@bitbucket.org/Chream/emacs.git"
                                  :fetcher git
                                  :files ("packages/ce-buffers.el")))
    (ce-key-bindings :location (recipe :url "https://Chream@bitbucket.org/Chream/emacs.git"
                                       :fetcher git
                                       :files ("packages/ce-key-bindings.el")))
    (ce-smartparens :location (recipe :url "https://Chream@bitbucket.org/Chream/emacs.git"
                                      :fetcher git
                                      :files ("packages/ce-smartparens.el")))
    (ce-text :location (recipe :url "https://Chream@bitbucket.org/Chream/emacs.git"
                               :fetcher git
                               :files ("packages/ce-text.el")))
    (ce-tools :location (recipe :url "https://Chream@bitbucket.org/Chream/emacs.git"
                                :fetcher git
                                :files ("packages/ce-tools.el")))
    (ce-rcirc :location (recipe :url "https://Chream@bitbucket.org/Chream/emacs.git"
                                :fetcher git
                                :files ("packages/ce-rcirc.el")))
    (whole-line-or-region (recipe :url "https://github.com/purcell/whole-line-or-region.git"
                                  :fetcher git))
    smartparens
    fold-dwim
    geiser))


(defun ce/init-ce-b
    etter-defaults ()
  (use-package ce-better-defaults
    :config (progn
              (ce-init-startup)
              (setq-default abbrev-mode t)
              (setq save-abbrevs t)
              (cl-dolist (env-var '("LC_ALL"
                                    "GRADLE_HOME"
                                    "ECLIPSE_HOME"
                                    "NLTK_DATA"
                                    "PYTHONIOENCODING"
                                    "GTAGSLABEL"
                                    ))
                (add-to-list 'exec-path-from-shell-variables env-var))
              (exec-path-from-shell-initialize))))

(defun ce/init-ce-files ()
  (use-package ce-files
    :defer t))

(defun ce/init-ce-common-lisp ()
  (use-package ce-files
    :defer t))

(defun ce/init-ce-java ()
  (use-package ce-java
    :defer t))

(defun ce/init-ce-buffers ()
  (use-package ce-buffers
    :defer t))

(defun ce/init-ce-key-bindings ()
  (use-package ce-key-bindings
    :init (progn
            ;; translate some keys.
            (define-key key-translation-map (kbd "-") (kbd "@"))
            (define-key key-translation-map (kbd "@") (kbd "-"))
            (define-key key-translation-map (kbd "C-@") (kbd "/")))

    :config (progn
              ;; Setup global keys.
              ;; https://stackoverflow.com/questions/683425/globally-override-key-binding-in-emacs
              (add-hook 'minibuffer-setup-hook (lambda () (mgkb-minor-mode 0)))
              ;; (add-hook 'after-load-functions 'mgkb-have-priority)

              ;; Setup common lisp slime key bindings.
              (setf my-lisp-key-hooks '(lisp-mode-hook
                                        common-lisp-mode-hook))

              (cl-dolist (mode-hook my-lisp-key-hooks)
                (add-hook mode-hook (lambda () (lisp-keys-mode 1))))

              )))

(defun ce/init-ce-smartparens ()
  (use-package ce-smartparens
    :defer t))

(defun ce/init-ce-rcirc ()
  (use-package ce-rcirc
    :defer t
    :config (progn
              (add-hook 'rcirc-mode-hook
                        (lambda ()
                          (if (string-match (regexp-opt '("chat.freenode.net")
                                                        (buffer-name))
                                            (rcirc-reconnect-mode 1))))))))

(defun ce/post-init-smartparens ()
  ;; (sp-pair "<" ">" :wrap "C-<")
  ;; (sp-pair "(" ")" :wrap "C-å")
  ;; (sp-pair "[" "]" :wrap "C-æ")
  ;; (sp-pair "{" "}" :wrap "C-o")
  ;; (sp-pair "`" "'" :wrap "C-´")
  )

(defun ce/init-ce-text ()
  (use-package ce-text
    :defer t))

(defun ce/init-ce-tools ()
  (use-package ce-tools
    :defer t))

(defun ce/init-fold-dwim ()
  (use-package fold-dwim
    :commands (fold-dwim-toggle)
    :config (progn
              (eval-after-load 'hideshow
                (hs-minor-mode 1)))))

;; Customize Geiser
(defun ce/pre-init-geiser ()
  (setq geiser-active-implementations '(chicken guile)))

(defun ce/init-whole-line-or-region ()
  (use-package whole-line-or-region
    :defer t))

;;; packages.el ends here
